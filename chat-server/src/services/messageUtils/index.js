function isObject(value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

function isString(value) {
  return typeof value === 'string' || value instanceof String;
}

export function getDataFromMessage(message) {
  try {
    const { type, data } = JSON.parse(message);
    return { type, data };
  } catch (e) {
    return { type: 'string', data: message };
  }
}

export function getMessageFromData(data) {
  if (isObject(data)) {
    return JSON.stringify(data);
  }

  if (isString(data)) {
    return data;
  }

  if (data.toString) {
    return data.toString();
  }

  throw new Error('Unsupported socket data type');
}
