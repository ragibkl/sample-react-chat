import { assign } from 'lodash';

export default (sequelize, DataTypes) => {
  const options = {
    comment: 'user',
  };

  const attributes = {
    name: {
      type: DataTypes.STRING(40),
      allowNull: false,
    },
  };

  const instanceMethods = {
    async broadcast(chat) {
      const user = await chat.getUser();

      const data = {
        type: 'chat-message',
        data: {
          ...chat.get({ plain: true }),
          user: user.get({ plain: true }),
        },
      };

      const users = await this.getUsers();
      await Promise.all(users.map(async u => {
        const sessions = await u.getSessions();
        await Promise.all(sessions.map(s => s.send(data)))
      }))
    },
  };

  const ChatRoom = sequelize.define('ChatRoom', attributes, options);

  ChatRoom.associate = (models, opts) => {
    ChatRoom.hasMany(models.Chat, {
      ...opts,
      foreignKey: 'chatRoomId',
    });
    ChatRoom.belongsToMany(models.User, {
      through: models.ChatRoomUser,
      foreignKey: 'chatRoomId',
      otherKey: 'userId',
    });
  };

  assign(ChatRoom.prototype, instanceMethods);

  return ChatRoom;
};
