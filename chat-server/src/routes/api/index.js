import { Router } from 'express';
import helloRouter from './hello';

const router = Router();

router.use('/', helloRouter);

export default router;
