// 'development' or 'production'
const NODE_ENV = process.env.NODE_ENV || 'development';

const API_PORTS = {
  development: 3001,
  production: 80,
};

const SOCKET_SERVICE_BASE_URLS = {
  development: 'http://socket-server:3001',
  production: 'http://socket-server',
};

export const API_PORT = API_PORTS[NODE_ENV];

export const REDIS_HOST = 'redis';
export const REDIS_PORT = '6379';

export const SOCKET_SERVICE_BASE_URL = SOCKET_SERVICE_BASE_URLS[NODE_ENV];

export default {
  API_PORT,
  REDIS_HOST,
  REDIS_PORT,
  SOCKET_SERVICE_BASE_URL,
};
