#!/bin/bash

source scripts/.env

if [[ $# -eq 0 ]] ; then
    echo "
Description: opens a shell inside the specified container
Usage: ./scripts/ssh.sh <container_name>

Examples:
./scripts/ssh.sh chat-server
./scripts/ssh.sh socket-server
./scripts/ssh.sh client
./scripts/ssh.sh mysql
"
    exit 0
fi

NAME=${1}
docker-compose exec $NAME bash
