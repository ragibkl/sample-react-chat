#!/usr/bin/env bash

source scripts/.env

docker-compose down -v
docker-compose run chat-server npm install
docker-compose run socket-server  npm install
