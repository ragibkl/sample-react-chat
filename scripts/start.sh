#!/usr/bin/env bash

source scripts/.env

docker-compose stop chat-server socket-server
docker-compose up -d
./scripts/logs.sh
