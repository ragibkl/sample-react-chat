// 'development' or 'production'
const NODE_ENV = process.env.NODE_ENV || 'development';

const WS_PORTS = {
  development: 3080,
  production: 3080,
};

const API_PORTS = {
  development: 3001,
  production: 80,
};

const CHAT_SERVICE_BASE_URLS = {
  development: 'http://chat-server:3001',
  production: 'http://chat-server',
};

export const WS_PORT = WS_PORTS[NODE_ENV];
export const API_PORT = API_PORTS[NODE_ENV];

export const REDIS_HOST = 'redis';
export const REDIS_PORT = '6379';

export const CHAT_SERVICE_BASE_URL = CHAT_SERVICE_BASE_URLS[NODE_ENV];

export default {
  WS_PORT,
  API_PORT,
  REDIS_HOST,
  REDIS_PORT,
  CHAT_SERVICE_BASE_URL,
};
