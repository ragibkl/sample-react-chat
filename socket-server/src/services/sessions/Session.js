import uuid from 'uuid';
import Chance from 'chance';
import { pick, toPairs, flatten } from 'lodash';

import client from 'src/api/redisClient';
import { send, terminate } from 'src/api/socket';
import MessageQueue from './MessageQueue';

const PREFIX = 'session::sessions::';
const EXPIRY = 600;
const chance = new Chance();

function generateId() {
  return uuid.v4();
}

function generateToken() {
  return chance.string({ length: 60 });
}

function getKey(id) {
  return `${PREFIX}${id}`;
}

export default class Session {
  static async findById(id) {
    const key = getKey(id);
    const exists = await client.existsAsync(key);
    if (exists) {
      const data = await client.hgetallAsync(key);
      return new Session(data);
    }

    return null;
  }

  static async findByIdAndToken(tokenStr) {
    if (!tokenStr) return null;

    const [id, token] = tokenStr.split('__');
    const session = await Session.findById(id);

    if (session && session.token === token) {
      return session;
    }

    return null;
  }

  static create(socket) {
    const data = {
      id: generateId(),
      token: generateToken(),
      socketId: socket.id,
    };

    const session = new Session(data);
    setTimeout(() => session.save(), 0);

    return session;
  }

  constructor(data) {
    const { id, token, socketId } = data;

    this.id = id;
    this.token = token;
    this.socketId = socketId;
    this.key = `${PREFIX}${id}`;
  }

  get tokenString() {
    return `${this.id}__${this.token}`;
  }

  async save() {
    const data = pick(this, ['id', 'token', 'socketId']);
    const dataList = flatten(toPairs(data));

    await client.hmsetAsync(this.key, dataList);
  }

  async onResume(socket) {
    this.socketId = socket.id;
    await this.save();

    setTimeout(() => {
      const q = new MessageQueue(this.id);
      q.resolve(m => this.send(m));
    }, 10);

    await this.clearExpiry();
  }

  send(message) {
    if (this.socketId) {
      console.log({ socketId: this.socketId, message })
      send(this.socketId, message);
    } else {
      this.enqueueMessage(message);
    }
  }

  async close() {
    terminate(this.socketId);
    this.socketId = '';
    await this.save();
    await this.setExpiry();
  }

  async enqueueMessage(message) {
    const q = new MessageQueue(this.id);
    q.enqueue(message);
  }

  async setExpiry() {
    await client.expireAsync(this.key, EXPIRY);
  }

  async clearExpiry() {
    await client.persistAsync(this.key);
  }
}
