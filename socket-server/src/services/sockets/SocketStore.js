import { values } from 'lodash';

export class SocketStore {
  byId = {};

  get sockets() {
    return values(this.byId);
  }

  add(socket) {
    this.byId[socket.id] = socket;
  }

  remove({ id }) {
    this.byId[id] = undefined;
  }

  getById(id) {
    return this.byId[id];
  }
}

const socketStore = new SocketStore();

export default socketStore;
