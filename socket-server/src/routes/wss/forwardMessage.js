import { forwardSessionMessage } from 'src/api/chatService';

export default async function forwardMessage(socket, type, data) {
  console.log({ socketId: socket.id, data })
  await forwardSessionMessage(socket.sessionId, type, data);
}
