import Session from 'src/services/sessions/Session';

export default async function resumeSession(socket, tokenStr) {
  let session = await Session.findByIdAndToken(tokenStr);

  if (session) {
    session.onResume(socket);
    console.log('found existing session!');
  } else {
    console.log('creating new session!');
    session = Session.create(socket);
  }

  socket.sessionId = session.id; // eslint-disable-line no-param-reassign
  socket.sendJson({
    type: 'session-set-token',
    data: session.tokenString,
  });
}
