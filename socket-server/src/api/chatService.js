import axios from 'axios';

import { CHAT_SERVICE_BASE_URL as BASE_URL } from 'src/settings';

export function forwardSessionMessage(sessionId, type, data) {
  const url = `${BASE_URL}/service/sessions/${sessionId}/${type}`;
  return axios.post(url, { data });
}

export function deleteSession(sessionId) {
  const url = `${BASE_URL}/service/sessions/${sessionId}`;
  return axios.delete(url);
}
