import client from 'src/api/redisClient';

const SEND_CHANNEL = 'websocket-send:';
const RPC_CHANNEL = 'websocket-rpc';

function callMethod(method, socketId, data) {
  const message = JSON.stringify({ method, socketId, data });
  client.publish(RPC_CHANNEL, message);
}

export function send(socketId, message) {
  client.publish(`${SEND_CHANNEL}${socketId}`, message);
}

export function sendJson(socketId, data) {
  callMethod('sendJson', socketId, data);
}

export function terminate(socketId) {
  callMethod('terminate', socketId);
}
