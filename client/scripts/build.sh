#!/usr/bin/env bash
source .env
TAG=${1:-latest}

echo $IMAGE_REGISTRY:$TAG

# Build the react app, and copy to docker folder
npm run build
cp -r build/. docker/build

docker build -t $IMAGE_REGISTRY:$TAG docker
docker push $IMAGE_REGISTRY:$TAG

# Clean up the build artifacts
rm -rf build
rm -rf docker/build
