import autoBind from 'auto-bind';

import PingInterval from './PingInterval';
import MessageQueue from './MessageQueue';
import { runWhenOffline, runWhenOnline } from './utils';

const RECONNECT_MAX_TRIES = 30;
const RECONNECT_INTERVAL = 5000; // miliseconds
const RECONNECT_INTERVAL_IDLE = 15000; // miliseconds

const READY_STATE = {
  CONNECTING: 0,
  OPEN: 1,
  CLOSING: 2,
  CLOSED: 3,
};

const PING_MESSAGE = 'ping';
const PONG_MESSAGE = 'pong';

function getReconnectInterval() {
  return document.hidden ? RECONNECT_INTERVAL_IDLE : RECONNECT_INTERVAL;
}

class ReconnectingWebSocket {
  get readyState() {
    if (!this.socket) {
      return READY_STATE.CLOSED;
    }
    return this.socket.readyState;
  }

  constructor(url) {
    this.url = url;
    this.connectionFails = 0;
    this.isPing = false;
    this.isConnecting = false;
    this.messageQueue = new MessageQueue();
    this.pingInterval = new PingInterval(this.ping);
    autoBind(this);
    this.connect();
  }

  close() {
    this.clearPingInterval();
    if (this.socket) {
      this.socket.onopen = undefined;
      this.socket.onerror = undefined;
      this.socket.onmessage = undefined;
      this.socket.onclose = undefined;

      this.socket.close();
      delete this.socket;
      this.socket = undefined;

      if (this.onclose) {
        this.onclose();
      }
    }
  }

  open() {
    this.socket = new WebSocket(this.url);
    this.socket.onerror = this.handleOnConnectionError;
    this.socket.onopen = this.handleOnOpen;
    this.socket.onmessage = this.handleOnMessage;
  }

  connect() {
    console.log('ReconnectingWebSocket::connect');
    if (this.isConnecting) {
      return;
    }
    this.isConnecting = true;
    this.close();
    runWhenOnline(this.open);
  }

  handleOnConnectionError(e) {
    console.log('ReconnectingWebSocket::handleOnConnectionError');
    console.log(e);
    this.isConnecting = false;
    this.connectionFails += 1;
    if (RECONNECT_MAX_TRIES && this.connectionFails >= RECONNECT_MAX_TRIES) {
      return;
    }

    // reconnect
    setTimeout(this.connect, getReconnectInterval());
  }

  handleOnOpen(e) {
    console.log('handleOnOpen');
    this.isConnecting = false;

    this.socket.onclose = this.handleOnClose;
    this.socket.onerror = this.handleOnError;

    // run onopen handler
    if (this.onopen) {
      this.onopen(e);
    }

    // resolve existing messageQueue
    this.messageQueue.resolve(this.send);

    // start pinging the socket server
    this.setPingInterval();

    // add listener when browser goes offline
    runWhenOffline(this.connect);
    this.connectionFails = 0;
  }

  handleOnClose(e) {
    console.log('handleOnClose');
    if (this.onclose) {
      this.onclose(e);
    }
    this.connect();
  }

  handleOnError(e) {
    console.log('handleOnError');
    if (this.onerror) {
      this.onerror(e);
    }
    this.connect();
  }

  handleOnMessage(e) {
    if (e.data === PONG_MESSAGE) {
      this.onPong();
      return;
    }

    if (this.onmessage) {
      this.onmessage(e);
    }
  }

  send(data) {
    const { isPing, readyState } = this;
    if (isPing || readyState !== READY_STATE.OPEN) {
      console.log('ReconnectingWebSocket::send', { isPing, readyState, data });
      this.messageQueue.enqueue(data);
      return;
    }
    this.socket.send(data);
  }

  ping() {
    this.isPing = true;
    this.socket && this.socket.send && this.socket.send(PING_MESSAGE);
  }

  onPong() {
    this.isPing = false;
    this.messageQueue.resolve(this.send);
  }

  clearPingInterval() {
    this.isPing = false;
    this.pingInterval.stop();
  }

  setPingInterval() {
    this.isPing = false;
    this.pingInterval.start();
  }
}

export default ReconnectingWebSocket;
