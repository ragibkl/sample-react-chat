const STORAGE = sessionStorage;
const PREFIX = 'SESSION_TOKEN_';

function getKeyForUrl(url) {
  return `${PREFIX}${url}`;
}

const tokenStorage = {
  getItem(url) {
    const key = getKeyForUrl(url);
    return STORAGE.getItem(key);
  },
  setItem(url, token) {
    const key = getKeyForUrl(url);
    return STORAGE.setItem(key, token);
  },
};

const tokenStorageProxy = new Proxy(tokenStorage, {
  get(obj, property) {
    if (obj[property]) return obj[property];
    return obj.getItem(property);
  },
  set(obj, property, value) {
    if (obj[property]) throw new Error('object override not allowed');
    return obj.setItem(property, value);
  },
});

export default tokenStorageProxy;
