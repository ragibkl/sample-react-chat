import autoBind from 'auto-bind';

import MessageQueue from './MessageQueue';
import ReconnectingWebSocket from './ReconnectingWebSocket';
import { getMessageFromData } from './utils';
import tokenStorage from './tokenStorage';

class SessionSocket {
  get sessionToken() {
    return tokenStorage.getItem(this.url);
  }

  set sessionToken(value) {
    tokenStorage.setItem(this.url, value);
  }

  constructor(url) {
    this.url = url;
    this.isFirstOpen = true;
    this.messageQueue = new MessageQueue();
    autoBind(this);
    this.connect();
  }

  close() {
    if (this.socket) {
      this.socket.close();
      this.socket = undefined;
    }
  }

  connect() {
    this.close();
    this.socket = new ReconnectingWebSocket(this.url);
    this.socket.onopen = this.handleOnOpen;
    this.socket.onclose = this.handleOnClose;
    this.socket.onmessage = this.handleOnMessage;
  }

  handleOnOpen() {
    const data = { type: 'session-resume', data: this.sessionToken };
    this.socket.send(getMessageFromData(data));
  }

  handleOnMessage(event) {
    const msgObj = JSON.parse(event.data);
    console.log('SessionSocket::handleOnMessage', msgObj);

    const { data, type } = msgObj;
    switch (type) {
      case 'session-set-token':
        this.sessionToken = data;

        if (this.isFirstOpen) {
          this.isFirstOpen = false;
          if (this.onopen) {
            this.onopen();
          }
        }

        this.messageQueue.resolve(this.send);
        break;
      default:
        if (this.onmessage) {
          this.onmessage(msgObj);
        }
    }
  }

  send(data) {
    if (!this.socket || this.socket.readyState !== 1) {
      this.messageQueue.enqueue(data);
      return;
    }
    this.socket.send(getMessageFromData(data));
  }
}

export default SessionSocket;