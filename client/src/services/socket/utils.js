function isObject(value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

function isString(value) {
  return typeof value === 'string' || value instanceof String;
}

export function runWhenOnline(onlineTask) {
  if (navigator.onLine) {
    onlineTask();
    return;
  }

  const whenOnline = () => {
    window.removeEventListener('online', whenOnline);
    console.log('runWhenOnline::whenOnline');
    onlineTask();
  };
  window.addEventListener('online', whenOnline);
}

export function runWhenOffline(offlineTask) {
  if (!navigator.onLine) {
    offlineTask();
    return;
  }

  const whenOffline = () => {
    window.removeEventListener('offline', whenOffline);
    console.log('runWhenOffline::whenOffline');
    offlineTask();
  };
  window.addEventListener('offline', whenOffline);
}

export function getMessageFromData(data) {
  if (isObject(data)) {
    return JSON.stringify(data);
  }

  if (isString(data)) {
    return data;
  }

  if (data.toString) {
    return data.toString();
  }

  throw new Error('Unsupported socket data type');
}
