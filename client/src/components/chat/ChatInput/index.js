import React, { Component } from 'react';
import Textarea from 'react-textarea-autosize';

import styles from './styles.module.css';

const BUTTON_CHAR = '>';

export default class ChatInput extends Component {
  handleOnKeyPress = e => {
    const key = e.keyCode || e.which;

    // Enter key is pressed, without shiftKey
    if (key === 13 && !e.shiftKey) {
      e.preventDefault();
      this.onSubmit();
    }
  };

  handleDivRev = ref => {
    this.textAreaRef = ref;
  };

  onSubmit = () => {
    const { onSubmit } = this.props;
    this.focus();
    onSubmit();
  };

  focus() {
    this.textAreaRef.focus();
  }

  render() {
    const { onChange, text } = this.props;

    return (
      <div className={styles.container}>
        <p className={styles.helperText}>[shift] + [Enter] for multi-line</p>
        <div className={styles.inputRow}>
          <Textarea
            autoFocus
            className={styles.inputText}
            type="text"
            onChange={onChange}
            onKeyPress={this.handleOnKeyPress}
            inputRef={this.handleDivRev}
            value={text}
          />

          <button
            className={styles.submitButton}
            onClick={this.onSubmit}
            type="button"
          >
            {BUTTON_CHAR}
          </button>
        </div>
      </div>
    );
  }
}
