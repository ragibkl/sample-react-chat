import React, { Component } from 'react';

import ChatSocket from 'src/services/chat';
import ChatBox from './ChatBox';

export default class ChatBoxContainer extends Component {
  state = {
    text: '',
    chats: [],
    currentUser: {
      id: 0,
      name: 'anonymous',
    },
  };

  constructor(props) {
    super(props);

    this.socket = new ChatSocket({
      loadChats: this.loadChats,
      addChat: this.addChat,
      setCurrentUser: this.setCurrentUser,
    });
  }

  loadChats = chats => this.setState({ chats });

  addChat = chat => this.setState(({ chats }) => ({ chats: [...chats, chat] }));

  setCurrentUser = currentUser => this.setState({ currentUser });

  handleOnChangeText = event => this.setState({ text: event.target.value });

  handleOnSend = () => {
    const { text } = this.state;
    const data = text.trim();

    if (data) {
      this.socket.sendJson({ type: 'chat-message', data });
    }
    this.setState({ text: '' });
  };

  render() {
    const { chats, text, currentUser } = this.state;

    return (
      <ChatBox
        currentUser={currentUser}
        chats={chats}
        onSend={this.handleOnSend}
        onChangeText={this.handleOnChangeText}
        text={text}
      />
    );
  }
}
