import React, { Component } from 'react';

import Chat from 'src/components/chat';

import Header from './Header';
import styles from './styles.module.css';

class App extends Component {
  render() {
    return (
      <div className={styles.container}>
        <Header />
        <Chat />
      </div>
    );
  }
}

export default App;
