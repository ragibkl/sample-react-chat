import React from 'react';

import logo from './logo.svg';
import styles from './styles.module.css';

export default function functionName() {
  return (
    <header className={styles.header}>
      <img src={logo} className={styles.appLogo} alt="logo" />
      <p className={styles.headerText}>Sample React Chat</p>
    </header>
  );
}
